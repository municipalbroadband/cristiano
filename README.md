# Cristiano

Cristiano is, strictly, a framework for controlling lifecycle-oriented modules. In particular, it is used to implement a relay network that copies user messages between different chat platforms.

Each chat platform is implemented as a module. See the [`modules`](https://gitlab.com/municipalbroadband/cristiano/tree/master/src/modules) directory for a list of current modules.

## Concepts

### Hubs

The central concept in Cristiano is the **hub**. Hubs represent a complete network of message relays. Hub members receive all messages sent to the hub. For example, a Facebook Chat thread and a Discord channel might both subscribe to the same hub. When a message is posted to that hub, unless it is otherwise filtered by the subscribing modules, it will be sent to both Facebook Chat and Discord.

### Lifecycles

Lifecycles are long-lived processes that maintain certain state within Cristiano. For example, the Facebook Chat and Discord modules both maintain lifecycles that monitor their respective APIs for new messages from configured sources.

## Building

Cristiano can be built using [yarn](https://yarnpkg.com/). Once you have the `yarn` command installed, simply run:

```console
$ yarn
```

You can also ensure that your code follows linting rules:

```console
$ tslint --project .
```

## Configuring

There is a sample configuration file in the `etc` directory. You will need to customize it to your needs. Start by defining your hubs (which are arbitrary, and up to you to name), and then attach relay modules to them.

### Facebook Chat

Finding the thread ID of a Facebook Chat can only be done with the URL from the desktop browser. Go to [the desktop Messenger interface](https://www.facebook.com/messages/t/) and click on the thread you want to add Cristiano to. You will need to manually add his user to your thread, and then copy the thread ID from the end of the URL (the number after the `/messages/t/`) to the configuration file.

### Discord

To get the channel ID of a Discord channel, enable the developer experience for your Discord account. Then right click on any channel and click *Copy ID*.

## Running

Once Cristiano has been built, you can run him using the `node` runtime:

```console
$ node lib/index.js -c etc/your-config.yml
```

Note that you will need to rerun `yarn` any time you make code changes to rebuild the `lib` directory.

## Deployment

Cristiano natively supports Heroku as a deployment platform. When you push it to a Heroku-based repository with the nodejs buildpack, it creates a dyno called `bot` that needs to be enabled in your Heroku application. You can conveniently specify the entire configuration section as a single Heroku config variable, `CRISTIANO_CONFIG`.
