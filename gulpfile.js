const $ = require('gulp');
const babel = require('gulp-babel');
const merge = require('gulp-merge');
const sourcemaps = require('gulp-sourcemaps');
const typescript = require('gulp-typescript');

const project = typescript.createProject('tsconfig.json');

$.task('default', () => {
  const ts = project.src()
    .pipe(sourcemaps.init())
    .pipe(project());

  const dts = ts.dts.pipe($.dest('lib'));
  const js = ts.js
    .pipe(babel())
    .pipe(sourcemaps.write('.'))
    .pipe($.dest('lib'));

  return merge(dts, js);
});
