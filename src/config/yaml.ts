import { DEFAULT_FULL_SCHEMA, Schema, Type } from "js-yaml";

const EnvType = new Type("!env", {
  instanceOf: String,
  kind: "scalar",
  resolve(data): boolean {
    return typeof data === "string";
  },
  construct(data: string): string {
    return process.env[data] || "";
  },
});

export const CONFIG_SCHEMA = Schema.create(DEFAULT_FULL_SCHEMA, EnvType);
