import { discord, facebookChat } from "../modules";

export interface IHubConfigSource {
  readonly name?: string;
}

export interface IHubsConfigSource {
  readonly [id: string]: IHubConfigSource;
}

export interface IModulesConfigSource {
  readonly [id: string]: any;
}

export interface IConfigSource {
  readonly hubs: IHubsConfigSource;
  readonly modules: IModulesConfigSource;
}

export { CONFIG_SCHEMA } from "./yaml";

export class Configurator {
  public readonly hubs = new Map<string, IHubConfigSource>();

  public constructor(readonly source: IConfigSource) {
    Object.keys(source.hubs || Object.create(null)).forEach((id) => {
      this.hubs.set(id, source.hubs[id]);
    });
  }

  public module(name: "discord"): Promise<discord.Configurator>;
  public module(name: "facebook-chat"): Promise<facebookChat.Configurator>;
  public module(name: string): any {
    const {modules} = this.source;

    switch (name) {
    case "discord":
      return new Promise((resolve, reject) => (
        modules.discord
          ? resolve(new discord.Configurator(modules.discord))
          : reject(new Error("no module configuration for Discord"))
      ));
    case "facebook-chat":
    return new Promise((resolve, reject) => (
        modules["facebook-chat"]
          ? resolve(new facebookChat.Configurator(modules["facebook-chat"]))
          : reject(new Error("no module configuration for Facebook Chat"))
      ));
    }
  }
}
