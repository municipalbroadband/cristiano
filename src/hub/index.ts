import { EventEmitter } from "events";
import { IHubConfigSource } from "../config";

export interface ISource {
  readonly platform: string;
  readonly id: any;
}

export interface IMessage {
  readonly source: ISource;
  readonly user: string;
  readonly content: string;
}

export class Hub extends EventEmitter {
  public constructor(readonly source: IHubConfigSource) {
    super();
  }

  public on(event: "message", listener: (message: IMessage) => void): this;
  public on(event: string | symbol, listener: (...args: any[]) => void): this {
    return super.on(event, listener);
  }

  public emit(event: "message", message: IMessage): boolean;
  public emit(event: string | symbol, ...args: any[]): boolean {
    return super.emit(event, ...args);
  }
}
