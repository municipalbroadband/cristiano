export interface IEvent<T> {
  readonly target: T;
}

export type IEventCallbackFunc<T> = (event: IEvent<T>) => void;

export interface IEventProvider<T> {
  listen(callback: IEventCallbackFunc<T>): Promise<void>;
}
