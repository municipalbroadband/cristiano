export interface ILifecycleProvider {
  use(lifecycle: ILifecycle): Promise<IStartedLifecycle>;
}

export interface IStartedLifecycle {
  stop(): Promise<void>;
}

export interface ILifecycle {
  readonly description: string;

  start(supervisor: ILifecycleProvider): Promise<IStartedLifecycle>;
}
