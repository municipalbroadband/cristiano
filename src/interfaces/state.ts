export interface IState {
  at(key: string): IState;
  put(key: string, value: any): Promise<void>;
  get(key: string): Promise<any>;
}
