import "source-map-support/register";

import { CONFIG_SCHEMA, Configurator } from "./config";
import { Hub } from "./hub";
import { IState } from "./interfaces/state";
import { LifecycleProvider } from "./lifecycle";
import { discord, facebookChat } from "./modules";
import { RedisState } from "./state";

import * as program from "commander";
import * as fs from "fs";
import * as yaml from "js-yaml";
import * as winston from "winston";

program
  .version("0.1.0")
  .option("-c, --config <path>", "The path to the configuration file [config.yml]")
  .parse(process.argv);

// Set up state.
let state: IState | undefined;
if (process.env.REDIS_URL) {
  state = new RedisState(process.env.REDIS_URL);
}

// Load our configuration and set up needed hubs.
let source: string;
if (program.config) {
  source = fs.readFileSync(program.config, "utf8");
} else {
  source = process.env.CRISTIANO_CONFIG || "";
}

const config = new Configurator(yaml.load(source, {schema: CONFIG_SCHEMA}));

const hubs = new Map<string, Hub>();
config.hubs.forEach((opts, id) => {
  hubs.set(id, new Hub(opts));
});

// Our main lifecycle.
const lp = new LifecycleProvider("Cristiano");

// Add Discord hooks.
config.module("discord").then((dc) => {
  const builder = new discord.LifecycleBuilder(dc.token);

  dc.postToChannels.forEach((m) => {
    const hub = hubs.get(m.hub);
    if (!hub) {
      throw new Error(`Discord relay configuration references nonexistent hub "${m.hub}.`);
    }

    builder.interactions.push(new discord.interactions.PostToChannel(m.channel, hub));
  });

  dc.sendToHubs.forEach((m) => {
    const hub = hubs.get(m.hub);
    if (!hub) {
      throw new Error(`Discord relay configuration references nonexistent hub "${m.hub}.`);
    }

    builder.interactions.push(new discord.interactions.SendToHub(m.channel, hub));
  });

  lp.use(builder.build());
}).catch((err) => {
  winston.warn("Not configuring Discord: %s.", err);
});

config.module("facebook-chat").then((fcc) => {
  const builder = new facebookChat.LifecycleBuilder(fcc.credentials.email, fcc.credentials.password);
  builder.force = fcc.credentials.force;

  if (state) {
    builder.state = state.at("modules").at("facebook-chat");
  }

  fcc.postToThreads.forEach((m) => {
    const hub = hubs.get(m.hub);
    if (!hub) {
      throw new Error(`Facebook Chat relay configuration references nonexistent hub "${m.hub}.`);
    }

    builder.interactions.push(new facebookChat.interactions.PostToThread(m.thread, hub));
  });

  fcc.sendToHubs.forEach((m) => {
    const hub = hubs.get(m.hub);
    if (!hub) {
      throw new Error(`Facebook Chat relay configuration references nonexistent hub "${m.hub}.`);
    }

    builder.interactions.push(new facebookChat.interactions.SendToHub(m.thread, hub));
  });

  lp.use(builder.build());
}).catch((err) => {
  winston.warn("Not configuring Facebook Chat: %s.", err);
});

(async () => {
  await lp.start();

  winston.info("All lifecycles started.");
})();
