import { ILifecycle, ILifecycleProvider, IStartedLifecycle } from "../interfaces/lifecycle";
import { Deferred } from "../util/deferred";

import * as winston from "winston";

export class LifecycleProvider {
  private latch = new Deferred<void>();

  private lifecycles: Array<Promise<IStartedLifecycle>> = [];
  private children: LifecycleProvider[] = [];

  public constructor(readonly description: string, readonly parent?: LifecycleProvider) {
    if (parent) {
      parent.adopt(this);
    }
  }

  public use(lifecycle: ILifecycle): Promise<IStartedLifecycle> {
    const child = new LifecycleProvider(lifecycle.description, this);
    const p = new Promise<IStartedLifecycle>((resolve, reject) => {
      const init = this.latch.then(() => {
        winston.info(`Supervisor "${this.description}": Starting lifecycle "${lifecycle.description}".`);
        return lifecycle.start(child);
      });

      init.catch(reject);

      const supervised = init.then((started) => {
        return child.start().then(() => started);
      });

      return supervised.then(resolve, reject);
    });

    this.lifecycles.push(p);
    return p;
  }

  public start(): Promise<IStartedLifecycle[]> {
    this.latch.resolve();

    return Promise.all(this.lifecycles);
  }

  protected adopt(child: LifecycleProvider) {
    this.children.push(child);
  }
}
