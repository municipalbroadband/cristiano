import { IState } from "../interfaces/state";

import * as Redis from "ioredis";

export class AtState implements IState {
  protected constructor(protected readonly client: Redis.Redis,
                        protected readonly root: string) {}

  public at(key: string): IState {
    return new AtState(this.client, `${this.root}${key}/`);
  }

  public put(key: string, value: any): Promise<void> {
    this.client.set(`${this.root}${key}`, value);
    return Promise.resolve();
  }

  public get(key: string): Promise<any> {
    return new Promise((resolve, reject) => {
      this.client.get(`${this.root}${key}`, (err, res) => {
        if (err) {
          return reject(err);
        }

        resolve(res);
      });
    });
  }
}

export class State extends AtState implements IState {
  public constructor(url: string) {
    super(new Redis(url), "");
  }
}
