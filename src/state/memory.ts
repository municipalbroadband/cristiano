import { IState } from "../interfaces/state";

import * as Redis from "ioredis";

export class AtState implements IState {
  protected constructor(protected readonly map: Map<string, any>,
                        protected readonly root: string) {}

  public at(key: string): IState {
    return new AtState(this.map, `${this.root}${key}/`);
  }

  public put(key: string, value: any): Promise<void> {
    this.map.set(`${this.root}${key}`, value);
    return Promise.resolve();
  }

  public get(key: string): Promise<any> {
    return Promise.resolve(this.map.get(`${this.root}${key}`));
  }
}

export class State extends AtState implements IState {
  public constructor() {
    super(new Map<string, any>(), "");
  }
}
