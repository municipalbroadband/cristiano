import { IEvent, IEventCallbackFunc, IEventProvider } from "../interfaces/event";
import { Deferred } from "../util/deferred";

import { EventEmitter } from "events";

class Event<T> implements IEvent<T> {
  public constructor(readonly target: T) {}
}

class EventProvider<T> implements IEventProvider<T> {
  public constructor(readonly closer: Deferred<void>, readonly emitter: EventEmitter) {}

  public listen(callback: IEventCallbackFunc<T>): Promise<void> {
    this.emitter.on("emit", (event: Event<T>) => {
      callback(event);
    });

    return this.closer.promise;
  }
}

export class EventTrigger<T> {
  public readonly provider: IEventProvider<T>;

  private readonly closer = new Deferred<void>();
  private readonly emitter = new EventEmitter();

  public constructor() {
    this.closer.then(() => {
      this.emitter.removeAllListeners();
    });

    this.provider = new EventProvider(this.closer, this.emitter);
  }

  public emit(value: T): void {
    this.emitter.emit("emit", new Event<T>(value));
  }

  public close(): void {
    this.closer.resolve();
  }
}
