import { IEvent, IEventCallbackFunc, IEventProvider } from "../interfaces/event";
import { Deferred } from "../util/deferred";

class Event<T> implements IEvent<T> {
  public constructor(readonly target: T) {}
}

class EventProvider<T> implements IEventProvider<T> {
  public constructor(readonly deferred: Deferred<Event<T>>) {}

  public listen(callback: IEventCallbackFunc<T>): Promise<void> {
    this.deferred.then(callback);

    return this.deferred.then(() => undefined, () => undefined);
  }
}

export class EventTrigger<T> {
  private deferred = new Deferred<Event<T>>();

  public provider(): IEventProvider<T> {
    // Doesn't matter if nobody sees the rejection.
    this.deferred.catch(() => undefined);

    return new EventProvider(this.deferred);
  }

  public resolve(value: T): void {
    this.deferred.resolve(new Event<T>(value));
  }

  public close(): void {
    this.deferred.reject();
  }
}
