import { ILifecycle, ILifecycleProvider, IStartedLifecycle } from "../../interfaces/lifecycle";
import * as interactions from "./interactions";

import { Client, Message } from "discord.js";

export interface IInteraction {
  build(client: Client): ILifecycle;
}

interface IOptions {
  readonly interactions: IInteraction[];
  readonly token: string;
}

class StartedLifecycle implements IStartedLifecycle {
  public constructor(readonly client: Client) {}

  public stop(): Promise<void> {
    return Promise.resolve();
  }
}

class Lifecycle implements ILifecycle {
  public readonly description = "Discord Module";

  public constructor(readonly options: IOptions) {}

  public start(supervisor: ILifecycleProvider): Promise<IStartedLifecycle> {
    const client = new Client();

    return client.login(this.options.token).then(() => {
      this.options.interactions.forEach((i) => supervisor.use(i.build(client)));
      return new StartedLifecycle(client);
    });
  }
}

export class LifecycleBuilder {
  public readonly interactions: IInteraction[] = [];

  constructor(readonly token: string) {}

  public build(): ILifecycle {
    return new Lifecycle(this);
  }
}

export { Configurator } from "./config";
export { interactions };
