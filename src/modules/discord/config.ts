export interface IHubChannelMappingConfigSource {
  readonly hub: string;
  readonly channel: string;
}

export interface IConfigSource {
  readonly token: string;
  readonly relay?: IHubChannelMappingConfigSource[];
  readonly "post-to-channel"?: IHubChannelMappingConfigSource[];
  readonly "send-to-hub"?: IHubChannelMappingConfigSource[];
}

export class Configurator {
  public readonly token: string;
  public readonly postToChannels: IHubChannelMappingConfigSource[];
  public readonly sendToHubs: IHubChannelMappingConfigSource[];

  public constructor(readonly source: IConfigSource) {
    this.token = source.token;
    this.postToChannels = [...source["post-to-channel"] || []];
    this.sendToHubs = [...source["send-to-hub"] || []];

    (source.relay || []).forEach((mapping) => {
      this.postToChannels.push(mapping);
      this.sendToHubs.push(mapping);
    });
  }
}
