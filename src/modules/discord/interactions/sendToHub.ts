import { ILifecycle, ILifecycleProvider, IStartedLifecycle } from "../../../interfaces/lifecycle";

import { Client, Guild, GuildChannel, Message, TextChannel, User } from "discord.js";
import { Hub, IMessage, ISource } from "../../../hub";

export const PLATFORM = "discord";

function userDisplayName(user: User, guild?: Guild): string {
  if (!guild) {
    return user.username;
  }

  const details = guild.members.get(user.id);
  if (!details) {
    return user.username;
  }

  return details.nickname || user.username;
}

class HubMessage implements IMessage {
  public readonly source: ISource;
  public readonly user: string;
  public readonly content: string;

  public constructor(readonly slc: StartedLifecycle, private readonly message: Message) {
    this.source = slc.source;

    const client = slc.client;

    const text = message.mentions.users.reduce((content, mention) => {
      const displayName = userDisplayName(mention, message.guild);
      return content.replace(`<@${mention.id}>`, `@${displayName}`)
        .replace(`<@!${mention.id}>`, `@${displayName}`)
        .replace(`<@&${mention.id}>`, `@${displayName}`);
    }, message.content);

    this.user = userDisplayName(message.author, message.guild);
    this.content = text
      .replace(/<#(\d+)>/g, (_: any, channelId: string) => {
        const channel = client.channels.get(channelId);
        if (channel instanceof GuildChannel) {
          return `#${channel.name}`;
        }

        return "#deleted-channel";
      })
      .replace(/<@&(\d+)>/g, (_: any, roleId: string) => {
        const role = message.guild.roles.get(roleId);
        if (role) {
          return `@${role.name}`;
        }

        return "@deleted-role";
      });
  }
}

class StartedLifecycle implements IStartedLifecycle {
  public readonly source: ISource;

  public constructor(readonly channel: TextChannel, readonly hub: Hub, readonly client: Client) {
    this.source = {
      id: channel.id,
      platform: PLATFORM,
    };

    client.on("message", this.receiver);
  }

  public stop(): Promise<void> {
    this.client.removeListener("message", this.receiver);
    return Promise.resolve();
  }

  private receiver = (message: Message) => {
    if (message.channel.type !== "text") {
      return;
    }

    if (message.channel.id !== this.channel.id) {
      return;
    }

    if (message.member.id === this.client.user.id) {
      return;
    }

    this.hub.emit("message", new HubMessage(this, message));
  }
}

class Lifecycle implements ILifecycle {
  public readonly description: string;

  public constructor(readonly channel: string, readonly hub: Hub, readonly client: Client) {
    this.description = `Discord: Send to Hub (${channel} -> ${hub.source.name})`;
  }

  public start(supervisor: ILifecycleProvider): Promise<IStartedLifecycle> {
    return new Promise((resolve, reject) => {
      const channel = this.client.channels.get(this.channel);
      if (!channel) {
        return reject(new Error(`Channel ${this.channel} does not exist.`));
      } else if (!(channel instanceof TextChannel)) {
        return reject(new Error(`Channel ${this.channel} is not a text channel.`));
      }

      return resolve(new StartedLifecycle(channel, this.hub, this.client));
    });
  }
}

export class Interaction {
  public constructor(readonly channel: string, readonly hub: Hub) {}

  public build(client: Client): ILifecycle {
    return new Lifecycle(this.channel, this.hub, client);
  }
}
