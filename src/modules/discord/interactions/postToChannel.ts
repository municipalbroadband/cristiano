import { ILifecycle, ILifecycleProvider, IStartedLifecycle } from "../../../interfaces/lifecycle";

import { Client, Guild, GuildChannel, Message, TextChannel, User } from "discord.js";
import { Hub, IMessage, ISource } from "../../../hub";
import { PLATFORM } from "./sendToHub";

class StartedLifecycle implements IStartedLifecycle {
  public constructor(readonly channel: TextChannel, readonly hub: Hub, readonly client: Client) {
    hub.on("message", this.receiver);
  }

  public stop(): Promise<void> {
    this.hub.removeListener("message", this.receiver);
    return Promise.resolve();
  }

  private receiver = (message: IMessage) => {
    if (message.source.platform === PLATFORM && message.source.id === this.channel.id) {
      return;
    }

    this.channel.send(`**<${message.user}>** ${message.content}`);
  }
}

class Lifecycle implements ILifecycle {
  public readonly description: string;

  public constructor(readonly channel: string, readonly hub: Hub, readonly client: Client) {
    this.description = `Discord: Post to Channel (${hub.source.name} -> ${channel})`;
  }

  public start(supervisor: ILifecycleProvider): Promise<IStartedLifecycle> {
    return new Promise((resolve, reject) => {
      const channel = this.client.channels.get(this.channel);
      if (!channel) {
        return reject(new Error(`Channel ${this.channel} does not exist.`));
      } else if (!(channel instanceof TextChannel)) {
        return reject(new Error(`Channel ${this.channel} is not a text channel.`));
      }

      return resolve(new StartedLifecycle(channel, this.hub, this.client));
    });
  }
}

export class Interaction {
  public constructor(readonly channel: string, readonly hub: Hub) {}

  public build(client: Client): ILifecycle {
    return new Lifecycle(this.channel, this.hub, client);
  }
}
