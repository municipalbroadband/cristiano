import { ILifecycle, ILifecycleProvider, IStartedLifecycle } from "../../../interfaces/lifecycle";

import { Collection } from "discord.js";
import { EventEmitter } from "events";
import "facebook-chat-api";
import { IAPI } from "../interfaces/api";

export interface IInteraction {
  build(api: IAPI): ILifecycle;
}

class UserStore extends Collection<string, Schmavery.FacebookChatAPI.IUserInfo> {
  public constructor(readonly api: Schmavery.FacebookChatAPI.IAPI) {
    super();
  }

  public fetch(id: string, cache = true): Promise<Schmavery.FacebookChatAPI.IUserInfo> {
    return new Promise((resolve, reject) => {
      if (cache) {
        const existing = this.get(id);
        if (existing) {
          return resolve(existing);
        }
      }

      this.api.getUserInfo(id, (err, obj) => {
        if (err) {
          return reject(err);
        }

        const user = obj[id];

        if (cache) {
          this.set(id, user);
        }

        return resolve(user);
      });
    });
  }
}

export class API extends EventEmitter implements IAPI {
  public readonly userID: string;
  private readonly userStore: UserStore;
  private closer: () => void;

  public constructor(private readonly delegate: Schmavery.FacebookChatAPI.IAPI) {
    super();

    this.userID = delegate.getCurrentUserID();
    this.userStore = new UserStore(delegate);
    this.closer = this.delegate.listen((err, e) => {
      if (err) {
        this.emit("error", err);
      } else {
        this.emit("received", e);
      }
    });
  }

  public close(): Promise<void> {
    return new Promise((resolve, reject) => {
      this.closer();
      resolve();
    });
  }

  public getUserInfo(id: string): Promise<Schmavery.FacebookChatAPI.IUserInfo> {
    return this.userStore.fetch(id);
  }

  public getThreadInfo(id: string): Promise<Schmavery.FacebookChatAPI.IThreadInfo> {
    return new Promise((resolve, reject) => {
      this.delegate.getThreadInfoGraphQL(id, (err, t) => {
        if (err) {
          return reject(err);
        }

        resolve(t);
      });
    });
  }

  public sendMessage(message: string | Schmavery.FacebookChatAPI.IMessage,
                     threadID: string | string[]): Promise<Schmavery.FacebookChatAPI.IMessageInfo> {
    return new Promise((resolve, reject) => {
      this.delegate.sendMessage(message, threadID, (err, res) => {
        if (err) {
          return reject(err);
        }

        resolve(res);
      });
    });
  }
}

export class StartedLifecycle implements IStartedLifecycle {
  public constructor(readonly api: API) {}

  public stop(): Promise<void> {
    return this.api.close();
  }
}

export class Lifecycle implements ILifecycle {
  public readonly description = "Facebook Chat: API";

  public constructor(readonly api: Schmavery.FacebookChatAPI.IAPI,
                     readonly interactions: IInteraction[]) {}

  public start(supervisor: ILifecycleProvider): Promise<StartedLifecycle> {
    return new Promise((resolve, reject) => {
      const api = new API(this.api);

      this.interactions.forEach((i) => supervisor.use(i.build(api)));
      resolve(new StartedLifecycle(api));
    });
  }
}
