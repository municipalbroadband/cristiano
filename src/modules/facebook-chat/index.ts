import { ILifecycle, ILifecycleProvider, IStartedLifecycle } from "../../interfaces/lifecycle";
import { IState } from "../../interfaces/state";
import { MemoryState } from "../../state";
import { API, IInteraction, Lifecycle as APILifecycle } from "./api";
import * as interactions from "./interactions";

import login = require("facebook-chat-api");

interface IOptions {
  readonly interactions: IInteraction[];
  readonly email: string;
  readonly password: string;
  readonly force: boolean;
}

class StartedLifecycle implements IStartedLifecycle {
  public stop(): Promise<void> {
    return Promise.resolve();
  }
}

class Lifecycle implements ILifecycle {
  public readonly description = "Facebook Chat";

  public constructor(readonly options: IOptions, readonly state: IState) {}

  public start(supervisor: ILifecycleProvider): Promise<IStartedLifecycle> {
    return new Promise((resolve, reject) => {
      this.state.get("state").then((state) => {
        const credentials = state
          ? {appState: JSON.parse(state)}
          : this.options;

        login(credentials, {forceLogin: this.options.force}, (err, res) => {
          if (err) {
            return reject(err);
          }

          this.state.put("state", JSON.stringify(res.getAppState())).then(() => {
            supervisor.use(new APILifecycle(res, this.options.interactions));
            resolve(new StartedLifecycle());
          }).catch((reason) => reject(reason));
        });
      }).catch((reason) => reject(reason));
    });
  }
}

export class LifecycleBuilder {
  public readonly interactions: IInteraction[] = [];
  public state: IState;
  public force: boolean = false;

  public constructor(readonly email: string, readonly password: string) {
    this.state = new MemoryState();
  }

  public build(): ILifecycle {
    return new Lifecycle(this, this.state);
  }
}

export { Configurator } from "./config";
export { interactions };
