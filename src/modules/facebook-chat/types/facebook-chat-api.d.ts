declare namespace Schmavery.FacebookChatAPI {
  export interface IError {
    readonly error: string;
  }

  export interface IAttachment {
    type: "sticker" | "file" | "photo" | "animated_image" | "share" | "video";
  }

  export interface IThreadInfo {
    threadID: string;
    participantIDs: Array<string>;
    formerParticipants: Array<string>;
    name: string;
    nicknames?: Array<string>;
    snippet: string;
    snippetHasAttachment: boolean;
    snippetAttachments: Array<IAttachment>;
    snippedSender: string;
    unreadCount: number;
    messageCount: number;
    imageSrc?: string;
    timestamp: number;
    serverTimestamp: number;
    muteSettings: any;
    isCanonicalUser: boolean;
    isCanonical: boolean;
    canonicalFbid: string;
    isSubscribed: boolean;
    rootMessageThreadingID: string;
    folder: string;
    isArchived: boolean;
    recipientsLoadable: boolean;
    hasEmailParticipant: boolean;
    readOnly: boolean;
    canReply: boolean;
    composerEnabled: boolean;
    blockedParticipants: Array<string>;
    lastMessageID: string;
    emoji?: string;
    color?: string;
    lastReadTimestamp: number;
  }

  export interface IReceived {
    type: "message" | "event" | "typ" | "read_receipt" | "read" | "message_reaction" | "presence";
  }

  export interface IReceivedMessage extends IReceived {
    type: "message";

    threadID: string;
    senderID: string;
    messageID: string;

    isGroup: boolean;
    body: string;
    attachments: IAttachment[];
  }

  export interface IReceivedEvent extends IReceived {
    type: "event";

    logMessageType: "log:subscribe" | "log:unsubscribe" | "log:thread-name" | "log:thread-color" | "log:thread-icon" | "log:user-nickname";
    logMessageData: string;
    logMessageBody: string;
    author: string;
    threadID: string;
  }

  export interface IReceivedTyp extends IReceived {
    type: "typ";
    isTyping: boolean;
    from: string;
    threadID: string;
    fromMobile: boolean;
  }

  export interface IReceivedReadReceipt extends IReceived {
    type: "read_receipt";
    reader: string;
    time: number;
    threadID: string;
  }

  export interface IReceivedRead extends IReceived {
    type: "read";
    threadID: string;
    time: number;
  }

  export interface IReceivedMessageReaction extends IReceived {
    type: "message_reaction";
    reaction: string;
    threadID: string;
    userID: string;
    senderID: string;
    messageID: string;
    offlineThreadingID: string;
    timestamp: number;
  }

  export interface IReceivedPresence extends IReceived {
    type: "presence";
    timestamp: number;
    userID: string;
    statuses: 0 | 2 | number;
  }

  export interface IMessage {
    body: string;
  }

  export interface IStickerMessage extends IMessage {
    sticker: string;
  }

  export interface IAttachmentMessage extends IMessage {
    attachment: ReadableStream | ReadableStream[];
  }

  export interface IUrlMessage extends IMessage {
    url: string;
  }

  export interface IEmojiMessage extends IMessage {
    emoji: string;
    emojiSize: "small" | "medium" | "large";
  }

  export interface IMention {
    id: string;
    tag: string;
    fromIndex?: number;
  }

  export interface IMentionsMessage extends IMessage {
    mentions: IMention[];
  }

  export interface IMessageInfo {
    threadID: string;
    messageID: string;
    timestamp: number;
  }

  export interface IDictionary<TValue> {
    [key: string]: TValue;
  }

  export interface IUserInfo {
    name: string;
    firstName: string;

    alternateName: string;
    vanity: string;

    thumbSrc: string;
    profileUrl: string;

    gender: string;
    type: string;

    isFriend: boolean;
    isBirthday: boolean;

    searchTokens: string[];
  }

  export interface IAPI {
    getCurrentUserID(): string;

    getAppState(): any;

    getThreadList(start: number,
                  end: number,
                  type: "inbox" | "pending" | "archived",
                  callback: (err: IError, arr: Array<IThreadInfo>) => void): void;

    getThreadInfoGraphQL(threadID: string, callback: (err: IError, thread: IThreadInfo) => void): void;

    getUserInfo(ids: string, callback: (err: IError, obj: IDictionary<IUserInfo>) => void): void;

    sendMessage(message: string | IMessage,
                threadID: string | Array<string>,
                callback: (err: IError, messageInfo: IMessageInfo) => void): void;

    listen(callback: (error: IError, event: IReceived) => void): () => void;
  }

  export interface ICredentials {
    readonly email: string;
    readonly password: string;
  }

  export interface IAppStateCredentials {
    readonly appState: any;
  }

  export interface ILoginOptions {
    readonly forceLogin?: boolean;
    readonly logLevel?: "silly" | "verbose" | "info" | "http" | "warn" | "error" | "silent";
  }

  export interface ILoginError extends IError {
    continue(code: string | number): void;
  }
}

declare module "facebook-chat-api" {
  function login(credentials: Schmavery.FacebookChatAPI.ICredentials | Schmavery.FacebookChatAPI.IAppStateCredentials,
                 callback: (err: Schmavery.FacebookChatAPI.ILoginError,
                            res: Schmavery.FacebookChatAPI.IAPI) => void): void;

  function login(credentials: Schmavery.FacebookChatAPI.ICredentials | Schmavery.FacebookChatAPI.IAppStateCredentials,
                 options: Schmavery.FacebookChatAPI.ILoginOptions,
                 callback: (err: Schmavery.FacebookChatAPI.ILoginError,
                            res: Schmavery.FacebookChatAPI.IAPI) => void): void;

  export = login;
}
