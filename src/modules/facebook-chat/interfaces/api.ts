import "facebook-chat-api";

export interface IAPI {
  readonly userID: string;

  getUserInfo(id: string): Promise<Schmavery.FacebookChatAPI.IUserInfo>;
  getThreadInfo(id: string): Promise<Schmavery.FacebookChatAPI.IThreadInfo>;

  sendMessage(message: string | Schmavery.FacebookChatAPI.IMessage,
              threadID: string | string[]): Promise<Schmavery.FacebookChatAPI.IMessageInfo>;

  on(event: "error", listener: (message: Schmavery.FacebookChatAPI.IError) => void): this;
  on(event: "received", listener: (message: Schmavery.FacebookChatAPI.IReceived) => void): this;

  removeListener(event: "error", listener: (message: Schmavery.FacebookChatAPI.IError) => void): this;
  removeListener(event: "received", listener: (message: Schmavery.FacebookChatAPI.IReceived) => void): this;
}
