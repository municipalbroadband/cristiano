import { Hub, IMessage, ISource } from "../../../hub";
import { ILifecycle, ILifecycleProvider, IStartedLifecycle } from "../../../interfaces/lifecycle";
import { IAPI } from "../interfaces/api";
import { PLATFORM } from "./sendToHub";

import * as winston from "winston";

class StartedLifecycle implements IStartedLifecycle {
  public constructor(readonly threadID: string, readonly hub: Hub, readonly api: IAPI) {
    hub.on("message", this.receiver);
  }

  public stop(): Promise<void> {
    this.hub.removeListener("message", this.receiver);
    return Promise.resolve();
  }

  private receiver = (message: IMessage) => {
    if (message.source.platform === PLATFORM && message.source.id === this.threadID) {
      return;
    }

    this.api.sendMessage({body: `*<${message.user}>* ${message.content}`}, this.threadID)
      .catch((err) => {
        winston.warn(`Failed to send message to Facebook Chat: ${err}.`);
      });
  }
}

class Lifecycle implements ILifecycle {
  public readonly description: string;

  public constructor(readonly thread: string, readonly hub: Hub, readonly api: IAPI) {
    this.description = `Facebook Chat: Post to Thread (${hub.source.name} -> ${thread})`;
  }

  public start(supervisor: ILifecycleProvider): Promise<IStartedLifecycle> {
    return Promise.resolve(new StartedLifecycle(this.thread, this.hub, this.api));
  }
}

export class Interaction {
  public constructor(readonly thread: string, readonly hub: Hub) {}

  public build(api: IAPI): ILifecycle {
    return new Lifecycle(this.thread, this.hub, api);
  }
}
