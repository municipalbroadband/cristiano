import { Hub, IMessage, ISource } from "../../../hub";
import { ILifecycle, ILifecycleProvider, IStartedLifecycle } from "../../../interfaces/lifecycle";
import { IAPI } from "../interfaces/api";

import * as winston from "winston";

export const PLATFORM = "facebook-chat";

function isReceivedMessage(e: Schmavery.FacebookChatAPI.IReceived): e is Schmavery.FacebookChatAPI.IReceivedMessage {
  return e.type === "message";
}

class HubMessage implements IMessage {
  public readonly user: string;
  public readonly content: string;

  public constructor(readonly source: ISource,
                     message: Schmavery.FacebookChatAPI.IReceivedMessage,
                     user?: Schmavery.FacebookChatAPI.IUserInfo) {
    if (user) {
      this.user = user.name;
    } else {
      this.user = "deleted-user";
    }

    this.content = message.body;
  }
}

class StartedLifecycle implements IStartedLifecycle {
  private readonly source: ISource;

  public constructor(readonly threadID: string, readonly hub: Hub, readonly api: IAPI) {
    this.source = {
      id: threadID,
      platform: PLATFORM,
    };

    api.on("received", this.receiver);
  }

  public stop(): Promise<void> {
    this.api.removeListener("received", this.receiver);
    return Promise.resolve();
  }

  private receiver = (e: Schmavery.FacebookChatAPI.IReceived) => {
    if (!isReceivedMessage(e)) {
      return;
    }

    if (e.senderID === this.api.userID) {
      return;
    }

    if (e.threadID !== this.threadID) {
      return;
    }

    this.api.getUserInfo(e.senderID).then((u) => {
      this.hub.emit("message", new HubMessage(this.source, e, u));
    }).catch((uerr) => {
      winston.warn(`Failed to fetch user information: ${uerr}.`);
      this.hub.emit("message", new HubMessage(this.source, e));
    });
  }
}

class Lifecycle implements ILifecycle {
  public readonly description: string;

  public constructor(readonly thread: string, readonly hub: Hub, readonly api: IAPI) {
    this.description = `Facebook Chat: Send to Hub (${thread} -> ${hub.source.name})`;
  }

  public start(supervisor: ILifecycleProvider): Promise<IStartedLifecycle> {
    return Promise.resolve(new StartedLifecycle(this.thread, this.hub, this.api));
  }
}

export class Interaction {
  public constructor(readonly thread: string, readonly hub: Hub) {}

  public build(api: IAPI): ILifecycle {
    return new Lifecycle(this.thread, this.hub, api);
  }
}
