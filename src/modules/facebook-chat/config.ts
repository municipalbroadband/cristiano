export interface ICredentialsSource {
  readonly email: string;
  readonly password: string;
  readonly force: boolean;
}

export interface IHubThreadMappingConfigSource {
  readonly hub: string;
  readonly thread: string;
}

export interface IConfigSource {
  readonly credentials: ICredentialsSource;
  readonly relay?: IHubThreadMappingConfigSource[];
  readonly "post-to-thread"?: IHubThreadMappingConfigSource[];
  readonly "send-to-hub"?: IHubThreadMappingConfigSource[];
}

export class Configurator {
  public readonly credentials: ICredentialsSource;
  public readonly postToThreads: IHubThreadMappingConfigSource[];
  public readonly sendToHubs: IHubThreadMappingConfigSource[];

  public constructor(readonly source: IConfigSource) {
    this.credentials = source.credentials;
    this.postToThreads = [...source["post-to-thread"] || []];
    this.sendToHubs = [...source["send-to-hub"] || []];

    (source.relay || []).forEach((mapping) => {
      this.postToThreads.push(mapping);
      this.sendToHubs.push(mapping);
    });
  }
}
