export class Deferred<T> {
  public readonly promise: Promise<T>;
  private $resolve!: (value?: T | PromiseLike<T>) => void;
  private $reject!: (reason?: any) => void;

  public constructor() {
    this.promise = new Promise((resolve, reject) => {
      this.$resolve = resolve;
      this.$reject = reject;
    });
  }

  public resolve(value?: T | PromiseLike<T>): void {
    this.$resolve(value);
  }

  public reject(reason?: any): void {
    this.$reject(reason);
  }

  public then<TResult1 = T, TResult2 = never>(
    onfulfilled?: ((value: T) => TResult1 | PromiseLike<TResult1>) | undefined | null,
    onrejected?: ((reason: any) => TResult2 | PromiseLike<TResult2>) | undefined | null,
  ): Promise<TResult1 | TResult2> {
    return this.promise.then(onfulfilled, onrejected);
  }

  public catch<TResult = never>(
    onrejected?: ((reason: any) => TResult | PromiseLike<TResult>) | undefined | null,
  ): Promise<T | TResult> {
    return this.promise.catch(onrejected);
  }
}
